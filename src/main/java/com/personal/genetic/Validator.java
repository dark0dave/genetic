package com.personal.genetic;

import static com.personal.genetic.Chromosome.addUp;

class Validator {
  static Boolean validate(String decodedString, Integer total) {

    boolean num = true;
    char ch = '*';

    for (int x = 0; x < decodedString.length(); x++) {
      ch = decodedString.charAt(x);

      // Did we follow the num-oper-num-oper-num patter
      if (num == !Character.isDigit(ch)) return false;

      // Don't allow divide by zero
      if (x > 0 && ch == '0' && decodedString.charAt(x - 1) == '/') return false;

      // Zero divide by number
      if (x < decodedString.length() - 1 && ch == '0' && decodedString.charAt(x + 1) == '/') return false;

      num = !num;
    }

    // Can't end in an operator
    if (!Character.isDigit(ch)) {
      return false;
    }

    return addUp(decodedString) == total;
  }
}
