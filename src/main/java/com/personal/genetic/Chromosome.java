package com.personal.genetic;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

// Genetic Algorithm Node
class Chromosome {

  double score = 0.0;
  private int chromoLen;
  final private Random rand = new Random();
  private StringBuffer chromo = new StringBuffer(chromoLen * 4);
  private StringBuffer decodeChromo = new StringBuffer(chromoLen * 4);
  final private static char[] ltable = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '-', '*', '/'};

  // Constructor that generates a random
  Chromosome(int target, int chromoLen) {
    this.chromoLen = chromoLen;

    // Create the full buffer
    for (int y = 0; y < chromoLen; y++) {
      // Generate a random binary integer
      String binString = Integer.toBinaryString(rand.nextInt(ltable.length));
      int fillLen = 4 - binString.length();

      // Fill to 4
      for (int x = 0; x < fillLen; x++) chromo.append('0');

      // Append the chromo
      chromo.append(binString);
    }

    // Score the new cromo
    score = scoreChromo(target);
  }

  final String decodeChromo() {

    // Create a buffer
    decodeChromo.setLength(0);

    // Loop throught the chromo
    for (int x = 0; x < chromo.length(); x += 4) {
      // Get the
      int idx = Integer.parseInt(chromo.substring(x, x + 4), 2);
      if (idx < ltable.length) decodeChromo.append(ltable[idx]);
    }

    // Return the string
    return decodeChromo.toString();
  }

  // Scores this chromosome
  final double scoreChromo(int target) {
    final int total = addUp(decodeChromo());
    if (total == target) return 0.0;
    return 1.0 / (target - total);
  }

  // Crossover bits
  final void crossOver(Chromosome other, double crossRate) {

    // Should we cross over?
    if (rand.nextDouble() > crossRate) return;

    // Generate a random position
    int pos = rand.nextInt(chromo.length());

    // Swap all chars after that position
    for (int x = pos; x < chromo.length(); x++) {
      // Get our character
      char tmp = chromo.charAt(x);

      // Swap the chars
      chromo.setCharAt(x, other.chromo.charAt(x));
      other.chromo.setCharAt(x, tmp);
    }
  }

  // Mutation
  final void mutate(double mutRate) {
    for (int x = 0; x < chromo.length(); x++) {
      if (rand.nextDouble() <= mutRate)
        chromo.setCharAt(x, (chromo.charAt(x) == '0' ? '1' : '0'));
    }
  }

  // Add up the contents of the decoded chromosome
  static int addUp(String decodedString) {
    int total = 0;

    // Find numbers and operators
    List<SimpleEntry<String, String>> parsed = parse(decodedString);

    String Operator = null;
    for (SimpleEntry pair : parsed) {
      if (total == 0 && pair.getKey().equals("Number")) {
        total = Integer.parseInt((String) pair.getValue());
        continue;
      }
      if (total != 0 && pair.getKey().equals("Operator")) {
        Operator = (String) pair.getValue();
        continue;
      }
      if (total != 0 && Operator != null && pair.getKey().equals("Number")) {
        int incomingValue = Integer.parseInt((String) pair.getValue());
        total = parseOperator(total, incomingValue, Operator);
      }
    }
    return total;
  }

  static List<SimpleEntry<String, String>> parse(String decodedString) {
    List<SimpleEntry<String, String>> parsed = new ArrayList<>();
    int startAt = 0;
    boolean isFirst = true;
    boolean isDigit = false;

    for (int ptr = 0; ptr < decodedString.length(); ptr++) {
      isDigit = Character.isDigit(decodedString.charAt(ptr));
      if (!isFirst && !isDigit) {
        parsed.add(pairCreator("Number", decodedString.substring(startAt, ptr)));
      }
      if (isDigit) {
        isFirst = false;
      } else {
        isFirst = true;
        startAt = ptr + 1;
        parsed.add(pairCreator("Operator", "" + decodedString.charAt(ptr)));
      }
    }

    if (isDigit) {
      parsed.add(pairCreator("Number", decodedString.substring(startAt)));
    }
    return parsed;
  }

  static int parseOperator(int total, int num, String operator) {
    switch (operator) {
      case "+": {
        total += num;
        break;
      }
      case "-": {
        total -= num;
        break;
      }
      case "*": {
        total *= num;
        break;
      }
      case "/": {
        if (num != 0) total /= num;
        break;
      }
    }
    return total;
  }

  private static SimpleEntry<String, String> pairCreator(String name, String value) {
    return new SimpleEntry<>(name, value);
  }
}