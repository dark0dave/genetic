package com.personal.genetic;

import java.util.*;

import static com.personal.genetic.Validator.validate;

public class GA {

  final static private int chromoLen = 5;
  final static private double crossRate = 0.7;
  final static private double mutRate = 0.001;
  final static private int poolSize = 40;    // Must be even

  public static void main(String[] args) {
    new GA().doIt(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
  }

  Set<String> doIt(int target, long maxIterations) {

    int gen = 0;

    // Create the pool
    Set<String> resultPool = new HashSet<>();
    Set<Chromosome> pool = new HashSet<>(poolSize);
    Set<Chromosome> newPool = new HashSet<>(poolSize);

    // Generate unique chromosomes in the pool
    for (int x = 0; x < poolSize; x++) pool.add(new Chromosome(target, chromoLen));

    // Loop until solution is found
    while (maxIterations > 0) {
      maxIterations--;
      // Clear the new pool
      newPool.clear();

      // Add to the generations
      gen++;

      // Loop until the pool has been processed
      for (int x = pool.size() - 1; x >= 0; x -= 2) {
        // Select two members
        Chromosome n1 = selectMember(pool);
        Chromosome n2 = selectMember(pool);

        // Cross over and mutate
        n1.crossOver(n2, crossRate);
        n1.mutate(mutRate);
        n2.mutate(mutRate);

        // Re-score the nodes
        n1.scoreChromo(target);
        n2.scoreChromo(target);

        // Check to see if either is the solution
        String decoded = n1.decodeChromo();
        if (validate(decoded, target)) {
          System.out.println("Generations: " + gen + "  Solution: " + decoded);
          resultPool.add(decoded);
        }
        decoded = n2.decodeChromo();
        if (validate(decoded, target)) {
          System.out.println("Generations: " + gen + "  Solution: " + decoded);
          resultPool.add(decoded);
        }

        // Add to the new pool
        newPool.add(n1);
        newPool.add(n2);
      }

      // Add the newPool back to the old pool
      pool.addAll(newPool);
    }
    return resultPool;
  }

  private Chromosome selectMember(Set<Chromosome> l) {
    // Get the total fitness
    final double total = l.stream().map(chromosome -> chromosome.score).reduce((a, b) -> a + b).get();
    final double slice = total * new Random().nextDouble();

    // Loop to find the node
    double tot = 0.0;
    Chromosome chrom = null;
    for (Chromosome node : l) {
      chrom = node;
      tot += node.score;
      if (tot >= slice) {
        l.remove(node);
        return node;
      }
    }
    l.remove(chrom);
    return chrom;
  }
}