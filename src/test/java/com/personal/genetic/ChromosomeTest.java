package com.personal.genetic;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ChromosomeTest {
  @Test
  public void addsUpSimpleCombo() {
    String decodedString = "88-72";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(16));
  }

  @Test
  public void addsCorrectlyByIgnoringOtherOperator() {
    String decodedString = "8-+2";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(10));
  }

  @Test
  public void returnsNumberWhenNoOperatorExists() {
    String decodedString = "0802";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(802));
  }

  @Test
  public void returnsZeroWhenNoNumberExists() {
    String decodedString = "-+/*";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(0));
  }

  @Test
  public void returnsOneNumberWhenOnlyOneExists() {
    String decodedString = "-+8*";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(8));
  }

  @Test
  public void ignoresLastSymbol() {
    String decodedString = "1+8*";
    int result = Chromosome.addUp(decodedString);
    assertThat(result, is(9));
  }
}