package com.personal.genetic;

import java.util.Set;
import org.junit.Test;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class GATest {
  @Test
  public void shouldRun() {
    final Integer binaryTestNumber = 00010;
    final Set<String> executableResults = new GA().doIt(binaryTestNumber, 1000);
    final Boolean result = executableResults.stream()
      .map(x -> Validator.validate(x, binaryTestNumber))
      .reduce((a, b) -> a == b).orElse(false);
    assertThat(result, is(true));
  }
}